# postgres-backup-s3

Backup PostgresSQL to S3 (supports periodic backups), built for postgres 12.4

## Usage

Docker:
```sh
$ docker run -e S3_ACCESS_KEY_ID=key -e S3_SECRET_ACCESS_KEY=secret -e S3_BUCKET=my-bucket -e S3_PREFIX=backup -e POSTGRES_DATABASE=dbname -e POSTGRES_USER=user -e POSTGRES_PASSWORD=password -e POSTGRES_HOST=localhost registry.gitlab.com/engrave/postgres-backup-s3
```

Docker Compose:
```yaml
postgres:
  image: postgres:12.4
  environment:
    POSTGRES_USER: user
    POSTGRES_PASSWORD: password

pgbackups3:
  image: registry.gitlab.com/engrave/postgres-backup-s3:latest
  links:
    - postgres
  environment:
    SCHEDULE: '@daily'
    S3_REGION: region
    S3_ACCESS_KEY_ID: key
    S3_SECRET_ACCESS_KEY: secret
    S3_BUCKET: my-bucket
    S3_PREFIX: backup
    POSTGRES_HOST: postgres
    POSTGRES_DATABASE: dbname
    POSTGRES_USER: user
    POSTGRES_PASSWORD: password
    POSTGRES_EXTRA_OPTS: '--schema=public --blobs'
    FILENAME: mydb.sql.gz # optional
```

Optional environment variables:
```
FILENAME: Filename of the backup (default: ${POSTGRES_DATABASE}_$(date +"%Y-%m-%dT%H:%M:%SZ").sql.gz, example mydb_2020-01-01T00:00:00Z.sql.gz)
```

### Automatic Periodic Backups

You can additionally set the `SCHEDULE` environment variable like `-e SCHEDULE="@daily"` to run the backup automatically.

More information about the scheduling can be found [here](http://godoc.org/github.com/robfig/cron#hdr-Predefined_schedules).

### S3 versioning with static filename

If you configure your s3 bucket to version your object, you can use static filename which will override the previous backup. This is useful if you want to keep only the latest backup. You can also configure some lifecycle rules to delete old backups.

### Restore

Backup files are in sql text format ziped with gzip, so you can restore them with `psql`:

```sh
gunzip -c db.sql.gz | psql -h localhost -U user dbname
```

If your database already exists, you need to drop it first and create it again:

```sh
dropdb -h localhost -U user dbname
createdb -h localhost -U user dbname
```
